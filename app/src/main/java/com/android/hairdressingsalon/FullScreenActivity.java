package com.android.hairdressingsalon;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

public class FullScreenActivity extends AppCompatActivity {
    private ImageView imageView;

    public void onCreate(Bundle savedInstaceState) {
        super.onCreate(savedInstaceState);
        setContentView(R.layout.full_screen);
        imageView = findViewById(R.id.image_full_screen);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
//        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().hide();
        getSupportActionBar().setTitle("ImageFullScreen");
        Intent intent = getIntent();
        int position = intent.getExtras().getInt("id");
        ImageAdabter imageAdabter = new ImageAdabter(this);
        imageView.setImageResource(imageAdabter.imageArrray[position]);
    }
}
