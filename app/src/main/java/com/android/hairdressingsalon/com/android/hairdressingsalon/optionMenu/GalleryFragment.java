package com.android.hairdressingsalon.com.android.hairdressingsalon.optionMenu;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import androidx.fragment.app.Fragment;

import com.android.hairdressingsalon.FullScreenActivity;
import com.android.hairdressingsalon.ImageAdabter;
import com.android.hairdressingsalon.R;

public class GalleryFragment extends Fragment {

    GridView gridView;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstateState){
        View view = inflater.inflate(R.layout.gallery_fragment, container, false);
        gridView = view.findViewById(R.id.grid_view);
        gridView.setAdapter(new ImageAdabter(getContext()));
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                Intent intent = new Intent(getActivity(), FullScreenActivity.class);
                intent.putExtra("id", position);
                startActivity(intent);
            }
        });
        return view;
    }
}
