package com.android.hairdressingsalon.com.android.hairdressingsalon.optionMenu;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;

import com.android.hairdressingsalon.R;

public class AboutFragment extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstateState){
        return inflater.inflate(R.layout.about_fragment, container, false);
    }
}
