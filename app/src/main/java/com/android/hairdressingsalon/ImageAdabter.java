package com.android.hairdressingsalon;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

public class ImageAdabter extends BaseAdapter {

    private Context context;
    public int [] imageArrray = {
            R.drawable.first, R.drawable.second,R.drawable.eight,R.drawable.eleven,
            R.drawable.first1, R.drawable.first2, R.drawable.first3, R.drawable.first21,
            R.drawable.first23, R.drawable.first25, R.drawable.first26, R.drawable.first27,
            R.drawable.first28, R.drawable.first29, R.drawable.first30, R.drawable.first31,
            R.drawable.first35, R.drawable.five, R.drawable.four, R.drawable.nine, R.drawable.seven
    };
    public ImageAdabter(Context context){
        this.context = context;
    }

    @Override
    public int getCount() {
        return imageArrray.length;
    }

    @Override
    public Object getItem(int position) {
        return imageArrray[position];
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        ImageView imageView = new ImageView(context);
        imageView.setImageResource(imageArrray[position]);
        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        imageView.setLayoutParams(new GridView.LayoutParams(340,350));
        return imageView;
    }
}
